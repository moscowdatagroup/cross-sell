FROM fedora:rawhide
RUN dnf install -y \
    community-mysql \
    python2-PyMySQL \
    numpy \
    python-sqlalchemy \
    python-pandas \
    python-scikit-learn \
    igraph \
    python-igraph \
    fabric

# TODO: move data that needs copy into subfolder
COPY . /cross_selling
WORKDIR /cross_selling
CMD ["/bin/bash"]
# CMD ["python",  "/cross_selling/server.py"]
EXPOSE 5000
