Cross selling model
----------------------------

Model for generating cross selling items and page for manual check of cross selling items suggests

Docker Install
--------------

For development environment:

* ./env_dev/compose up


Install
-------

To install you need simply to do

``python setup.py install``


To run on local server
----------------------

Before run you need to add credentials to src/cross_sell/config/default.py

``DATABASE_URI = <your_connection_string>``

Then create table in this database

``mysql -u <user_name> -p <db_name> < schema.sql``

If you haven't got pretrained data you can train it from scratch

``python train.py``

Then take data data/sku_descriptions.csv or put here your own file and upload it to db with following command

``python src/upload_data.py <filename>``

After it you will have samples uploaded in DB and you can start Flask application

``python src/server.py``

Then just open localhost:5000 and enjoy :)


Required data sources
---------------------

Current solution use some additional dictionaries - flat category tree

``src/dictionaries/all_cat_tree.txt``


System architecture
-------------------

.. image:: cross_selling_schema.png
