import os
DEBUG = True
DATABASE_URI = 'mysql+pymysql://localhost/test?charset=utf8'
PORT = 5000
HOST_NAME = '0.0.0.0'
PACKAGE_DIR = os.path.dirname(os.path.dirname(__file__))
DATA_DIR = os.path.join(os.path.dirname(os.path.dirname(PACKAGE_DIR)), 'data')
JSONSCHEMA_DIR = os.path.join(PACKAGE_DIR, 'jsonschema')


def datadir(path):
    return os.path.join(DATA_DIR, path)

def jsonschema_dir(path):
    return os.path.join(JSONSCHEMA_DIR, path)


##############
# Data Sources
##############

APRIORI_MAP = datadir('apriori_for_sku.json')
TOP_SELLERS = datadir('top_sellers_categories_map.json')
SKU_GRAPH = datadir('sku_graph_map.json')
CAT_1_MAP = datadir('cat_graph_map_lvl_1.json')
CAT_2_MAP = datadir('cat_graph_map_lvl_2.json')
CAT_3_MAP = datadir('cat_graph_map_lvl_3.json')
CAT_4_MAP = datadir('cat_graph_map_lvl_4.json')
CATEGORY_MAP = datadir('category_map.json')
CATEGORY_TREE = os.path.join(PACKAGE_DIR, 'dictionaries/all_cat_tree.csv')
CAT_1_APRIORI = datadir('apriori_for_categories_lvl_1.json')
CAT_2_APRIORI = datadir('apriori_for_categories_lvl_2.json')
CAT_3_APRIORI = datadir('apriori_for_categories_lvl_3.json')
CAT_4_APRIORI = datadir('apriori_for_categories_lvl_4.json')
