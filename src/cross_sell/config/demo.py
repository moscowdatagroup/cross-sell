from default import datadir

DEBUG = False
HOST_NAME = '0.0.0.0'
DATABASE_URI = 'mysql+pymysql://root:root@localhost/bob_mx_live?charset=utf8'
SKU_MAP = datadir('sku_map.txt')
PORT = 8000
