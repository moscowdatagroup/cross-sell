# coding: utf-8
from datetime import datetime

from sqlalchemy import Column, Integer, String, Enum, Text, TIMESTAMP, text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class SkuItem(Base):

    __tablename__ = 'cross_selling_suggests'
    id = Column(Integer, primary_key=True)
    sku = Column(String(255), unique=True, nullable=False)
    name = Column(String(255), nullable=False)
    cat_1 = Column(String(255))
    cat_2 = Column(String(255))
    cat_3 = Column(String(255))
    cat_4 = Column(String(255))
    image_url  = Column(String(255))
    updated_at = Column(TIMESTAMP, onupdate=datetime.utcnow)
    status = Column(Enum('-1', '0'),  nullable=False, default='0')


    def __init__(self, sku, name,  cat_1=None, cat_2=None, cat_3=None, cat_4=None,
        image_url=None,  status='0'):

        self.sku = sku
        self.name = name
        self.cat_1 = cat_1
        self.cat_2 = cat_2
        self.cat_3 = cat_3
        self.cat_4 = cat_4
        self.status = status
        self.image_url = image_url

    def __repr__(self):
        return '<SkuItem: sku %r, name %r >' % (self.sku, self.name)
