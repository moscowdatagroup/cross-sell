import pandas as pd
import numpy as np
from cross_sell.config.hyperparams import SKU_GRAPH_REDUCTION


def create_recs_for_categories(item_categories, top_sellers, categories_graph, category_map, number_of_recs = 5, per_category=2):
    # create structure: sku, score, sku_category, original_category

    recommendations = []
    recommendations_sku = set()

    for item in item_categories:
        if str(item) in categories_graph:
            suggested_categories = categories_graph[str(item)]['cats']
            original_degree = categories_graph[str(item)]['degree']
            max_link = np.max(suggested_categories.values())
            max_link = np.max([max_link, original_degree])
            min_link = np.min(suggested_categories.values())
            min_link = np.min([min_link, original_degree])
            ratio = float(max_link/min_link)


            # DONT suggest categories if original category is rare
            if ratio >= 4 and float(max_link)/original_degree >=4:
                continue

            for sug_category in suggested_categories.keys():

                # filter rare links 

                if ratio >= 4 and float(max_link)/suggested_categories[sug_category] >= 4:
                    continue

                if sug_category in top_sellers.keys():
                    top_sellers_sku_list = top_sellers[sug_category]

                    top_sellers_sku_list = sorted(top_sellers_sku_list.items(), key=lambda x: x[1], reverse=True) 

                    # reduce by category

                    for sku, freq in top_sellers_sku_list[0:2]:
                        variant = {'sku':sku, 'score':freq*suggested_categories[sug_category],
                        'sku_category': category_map[sug_category], 'original_category': category_map[str(item)],
                        'item_frequency': freq, 'sku_category_degree': suggested_categories[sug_category], 
                        'original_degree':original_degree }

                        if sku not in recommendations_sku:
                            recommendations.append(variant)
                            recommendations_sku.add(sku)

    result = sorted(recommendations, key=lambda k: k['score'], reverse=True)[0:number_of_recs+1]

    return result

def collect_recommendations_per_sku(sku, sku_apriori, sku_graph, top_sellers, categories_graph, 
    categories_apriori, number_of_recs = 5, items_per_category = 3):
    '''Combine different recommendations for one sku in one list and return it'''

    result = []

    # Item apriori
    if sku.sku in sku_apriori.keys():
        sku_apriori_recs = sku_apriori[sku.sku]
        total_items = np.sum(sku_apriori_recs.values())
        for item in sku_apriori_recs.keys():
            result.append({'type':'item_apriori', 'recommended_sku': item, 'source_sku': sku.sku, 'score': float(sku_apriori_recs[item])/total_items })

    # Item graph
    if sku.sku in sku_graph.keys():
        sku_graph_recs = sku_graph[sku.sku]
        item_degree = sku_graph_recs['degree']
        max_degree = np.max([np.max(sku_graph_recs['items'].values()), item_degree])
        for item in sku_graph_recs['items'].keys():
            score = SKU_GRAPH_REDUCTION*(float(sku_graph_recs['items'][item])*item_degree)/(max_degree*max_degree)
            result.append({'type':'item_graph', 'recommended_sku': item, 'source_sku': sku.sku, 'score': score})

    # Categories apriori
    for i in [4, 3, 2, 1]:
        cat_list = getattr(sku, 'cat_'+str(i))
        if cat_list != None:
            cat_list = eval(cat_list)
            for cat in cat_list:
                cat_apriori_recs = categories_apriori[i].get(unicode(cat))
                if cat_apriori_recs != None:
                    total_items = np.sum(cat_apriori_recs.values())
                    for recommended_category in cat_apriori_recs.keys():
                        top_seller_for_category = top_sellers.get(recommended_category)

                        top_sellers_sum = np.sum(top_seller_for_category.values())
                        for rec_sku in top_seller_for_category.keys():
                            category_discount = 2**(4-i)
                            score = float(top_seller_for_category[rec_sku])*cat_apriori_recs[recommended_category]/(top_sellers_sum*total_items*category_discount)
                            result.append({'type':'category_top_sellers', 'recommended_sku': rec_sku, 'source_sku': sku.sku, 'score': score,
                                'extras': {'recommended_category': recommended_category,
                                           'original_category': cat,
                                           'category_level': i,
                                           'category_weight': cat_apriori_recs[recommended_category],
                                           'top_seller_weight': top_seller_for_category[rec_sku]
                                }})

    # Sort recommendations 
    result = sorted(result, key=lambda k: k['score'], reverse=True)

    return result[0:number_of_recs] 
