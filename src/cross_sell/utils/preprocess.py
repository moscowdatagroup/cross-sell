# coding: utf-8
import pandas as pd
import re

def change_key_types(name):
	new_dict = {}
	old_dict = eval(name)
	for key in old_dict:
		new_dict[str(key)] = old_dict[key]
	return new_dict

def create_nice_file(filename):

	raw_df = pd.read_csv(filename)
	#raw_df['links_with'] = raw_df['links_with'].apply(lambda x: re.sub(ur'\W+', ' ', x, flags = re.UNICODE).rstrip().lstrip().split()).apply(repr)
	#raw_df['suggested_items'] = raw_df['suggested_items'].apply(lambda x: re.sub(ur'\W+', ' ', x, flags = re.UNICODE).rstrip().lstrip().split()).apply(repr)
	return raw_df

