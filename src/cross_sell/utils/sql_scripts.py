orders_sql = """ SELECT   Date,
                      SKUConfig,
                      OrderNum,
                      CustomerNum,
                      Price,
                      PCOne,
                      PCTwo,
                      ItemsInOrder
             FROM     A_Master
                       
""" 

categories_data_sql = """ SELECT fk_catalog_category,
                                 fk_catalog_config
                          FROM   catalog_config_has_catalog_category
"""

sku_data_sql =  """ SELECT name,
                           sku,
                           description,
                           status,
                           id_catalog_config,
                           fk_catalog_brand
                     FROM  catalog_config
                     WHERE status = 'active'
                        
"""

visibility_sql = """SELECT * FROM  items_activity"""


brand_sql = """ SELECT id_catalog_brand,
                       url_key
                FROM   catalog_brand
"""
