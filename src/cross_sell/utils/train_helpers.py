
from collections import Counter


def return_pairs(vect):
	'''Create unique pairs in list'''

	pair_list = []
	for i in range(len(vect)-1):
		for k in range(i+1, len(vect)):
			pair_list.append(tuple(sorted([vect[i], vect[k]])))
	return pair_list


def conv_to_sku(sku_list, skus_map):
    '''Convert ids to sku names'''
    return [skus_map[item] for item in sku_list]

def cat_sign(cat_list, categories):
    '''Distribution of categories that are in other categories list'''

    cnt = Counter()
    for item in cat_list:
        if item in categories:
            cnt.update([item])
    return dict(cnt)


def collect_categories(item_list, sku_category_map):
    '''Collect categories for all items'''

    cat_result = []
    for item in item_list:
        cat_result.extend(sku_category_map[item])
    return cat_result


def create_category_pair(pair, value, sku_category_map):
    '''Create category co-occurence pairs'''

    result = []
    left_categories = sku_category_map[pair[0]]
    right_categories = sku_category_map[pair[1]]
    for item_l in left_categories:
        for item_r in right_categories:
                cat_pair = sorted([item_l, item_r])
                result.append(tuple(cat_pair))
    return result*value

def count_top_n(data, top_sellers, n=5):
    '''Return top n top sold items'''

    cnt = Counter()
    for item in data:
        cnt.update([item]*top_sellers[item])
    return dict(cnt.most_common(n))

