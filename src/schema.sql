drop table if exists cross_selling_suggests;
create table cross_selling_suggests (
  id integer primary key auto_increment,
  sku varchar(255) not null,
  name varchar(255) not null,
  cat_1 varchar(255),
  cat_2 varchar(255),
  cat_3 varchar(255),
  cat_4 varchar(255),
  image_url varchar(255),
  updated_at timestamp null,
  status enum('-1', '0') not null default '0',
  unique (sku(255))
);