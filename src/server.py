#!/usr/bin/env python
# coding: utf-8
import json
import os
import random
import re
try:
    from functools import lru_cache
except ImportError:
    from backports.functools_lru_cache import lru_cache

import logging
logging.basicConfig(level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(message)s')
logger = logging.getLogger(__name__)

from cross_sell.config import hyperparams


import jsonschema
from time import time
from cross_sell.config import default as settings
from cross_sell.models import Base, SkuItem
from cross_sell.utils.helpers import create_recs_for_categories, collect_recommendations_per_sku
from flask import Flask, render_template, request
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

app = Flask(__name__)
app.config.from_object('cross_sell.config.default')
# app.config.from_envvar('CAT_PAGE_SETTING', silent=True)
# FIXME: find reason of closed connections
engine = create_engine(app.config['DATABASE_URI'], pool_recycle=300)

db_session = scoped_session(sessionmaker(bind=engine))
Base.query = db_session.query_property()

# LOAD DICTIONARIES
with open(app.config['APRIORI_MAP'], 'r') as outfile:
    sku_apriori = json.load(outfile)

with open(app.config['TOP_SELLERS'], 'r') as outfile:
    top_sellers = json.load(outfile)

categories_apriori = {}
for i in [1, 2, 3, 4]:
    with open(app.config['CAT_' + str(i) + '_APRIORI'], 'r') as outfile:
        categories_apriori[i] = json.load(outfile)

categories_graph = {}
for i in [1, 2, 3, 4]:
    with open(app.config['CAT_' + str(i) + '_MAP'], 'r') as outfile:
        categories_graph[i] = json.load(outfile)

with open(app.config['SKU_GRAPH'], 'r') as outfile:
    sku_graph = json.load(outfile)

with open(app.config['CATEGORY_MAP'], 'r') as outfile:
    category_names = json.load(outfile)


@lru_cache()
def get_total_item_count():
    return db_session.query(SkuItem).count()


@app.route('/')
def show_variant(sku=None):
    """Page for check recommendations list per one SKU"""

    sku =  request.args.get('sku')
    if sku is None:
        sku = ''

    if not len(sku):
        rand = random.randrange(0, get_total_item_count())
        item = db_session.query(SkuItem).get(rand)
    else:
        item = SkuItem.query.filter(SkuItem.sku.like('%' + str(sku) + '%')).first()

    if item is None:
        return render_template('no_data.html')


    context = {
    'item': item,
    'category_names': category_names
    }

    # create recs list
    recs_list = {
    'apriori': None,
    'cat_1': None,
    'cat_2': None,
    'cat_3': None,
    'cat_4': None,
    'sku_graph': None

    }

    # ADD categories recs

    item_categories = {}
    for i in range(1,5):
        category = getattr(item, 'cat_' + str(i))
        if category != None:
            category = eval(category)
            categories_pretty = []
            for cat in category:
                cat_pretty_item = {}
                cat_pretty_item['name'] = category_names[str(cat)]
                cat_pretty_item['id'] = cat
                cat_pretty_item['degree'] = None

                if str(cat) in categories_graph[i].keys():
                     cat_pretty_item['degree'] = categories_graph[i][str(cat)]['degree']

                categories_pretty.append(cat_pretty_item)
            recs_categories_temp = create_recs_for_categories(category, top_sellers, categories_graph[i], category_names)

            list_of_sku = [cat_rec['sku'] for cat_rec in recs_categories_temp]
            list_of_sku = dict(zip(list_of_sku, recs_categories_temp))

            for sku in SkuItem.query.filter(SkuItem.sku.in_(list_of_sku.keys())).all():
                list_of_sku[sku.sku]['image'] = sku.image_url
                list_of_sku[sku.sku]['name'] = sku.name

            if len(list_of_sku):
                recs_list['cat_' + str(i)] = sorted(list_of_sku.values(), key=lambda k: k['score'], reverse=True)

        else:
            categories_pretty = []

        item_categories['cat_' + str(i)] = categories_pretty

    # ADD apriori recs
    if item.sku in sku_apriori.keys():
        list_of_sku = []
        for sku in SkuItem.query.filter(SkuItem.sku.in_(sku_apriori[item.sku].keys())).all():
            freq_sku = {'sku': sku.sku, 'name':sku.name, 'image': sku.image_url, 'frequency': sku_apriori[item.sku][sku.sku] }
            list_of_sku.append(freq_sku)

        recs_list['apriori'] = sorted(list_of_sku, key=lambda k: k['frequency'], reverse=True)

    # ADD graph recs
    item_degree = None
    if item.sku in sku_graph.keys():
        list_of_sku = []

        for sku in SkuItem.query.filter(SkuItem.sku.in_(sku_graph[item.sku]['items'].keys())).all():
            freq_sku = {'sku': sku.sku, 'name':sku.name, 'image': sku.image_url, 'degree': sku_graph[item.sku]['items'][sku.sku] }
            list_of_sku.append(freq_sku)

        recs_list['sku_graph'] =  sorted(list_of_sku, key=lambda k: k['degree'], reverse=True)
        item_degree = sku_graph[item.sku]['degree']

    context['degree'] = item_degree
    context['item_categories'] = item_categories
    context['recommendations'] = recs_list

    return render_template('index.html', **context)


with open(settings.jsonschema_dir('recommend.json')) as f:
    recommend_schema = json.loads(f.read())


def jsonify(obj):
    return json.dumps(obj)


def get_recommendations_list(sku_list, num_of_recs):
    """Create list of recommendations"""

    all_items = []

    # save requested data metainformation
    items_metainformation = []

    for sku in sku_list:
        item = SkuItem.query.filter(SkuItem.sku.like(str(sku))).first()

        item_meta = {}
        item_meta['sku'] = sku
        item_meta['data'] = item

        if item != None:
            logger.debug('Requested item: {sku}, Cat1: {cat1}, Cat2:{cat2}, Cat3: {cat3}, Cat4:{cat4}'.format(sku=item.sku,
                cat1=item.cat_1, cat2=item.cat_2, 
                cat3=item.cat_3, cat4=item.cat_4))

            all_items.extend(collect_recommendations_per_sku(item, sku_apriori, sku_graph, top_sellers, categories_graph,
                categories_apriori, num_of_recs))
        else:
            logger.error('Requested sku: {sku} not found'.format(sku = sku))

        items_metainformation.append(item_meta)

    
    # sort duplicates of items
    all_items = sorted(all_items, key=lambda k: k['score'], reverse=True)
    sku_in_recommendations = set()
    items_cleaned = []
    for item in all_items:
        if item['recommended_sku'] not in sku_in_recommendations:
            items_cleaned.append(item)
            sku_in_recommendations.add(item['recommended_sku'])
    
    items_cleaned = sorted(items_cleaned, key=lambda k: k['score'], reverse=True)

    return (items_cleaned, items_metainformation)




@app.route('/api/cross_sell/recommend/0.1/', methods=['POST'])
def recommend():

    request.get_data()
    raw_data = request.data
    try:
        data = json.loads(raw_data)
    except ValueError as e:
        return jsonify({
            'log': [{'level': 'error', 'code': 'input_is_not_json', 'details':
                     unicode(e)}],
            'response': None
        })
    try:
        jsonschema.validate(data, recommend_schema)
    except jsonschema.ValidationError as e:
        return jsonify({
            'log': [{'level': 'error', 'code': 'validation_error',
                     'details': unicode(e)}],
            'response': None
        })

    debugMode = False
    
    if 'meta' in data.keys():
        if 'debug' in data['meta']:
            debugMode = True

    sku_list = [item['sku'] for item in data['request']['card']['items']]

    # get list of recommendations
    items_cleaned, _ = get_recommendations_list(sku_list, data['request']['num_recs'])

    # Filter by num of recs
    recommended = {item['recommended_sku']: item['score'] for item in items_cleaned[0:data['request']['num_recs']]}

    response = {
        'log': [],
        'response': {'recommended': recommended,
                     'num_recs': len(recommended) 
                    }
    }

    if debugMode:
        response['debug_info'] = {
            'requested_data': data['request'],
            'raw_recommendations': items_cleaned
        }

    return jsonify(response)


@app.route('/card_recommendations_check/')
def show_cross_sell():
    """Page for purchase card creation and recommendations check"""

    number_of_recommendations = request.args.get('num_recs')
    
    if number_of_recommendations == None or len(number_of_recommendations) == 0:
        number_of_recommendations = hyperparams.DEFAULT_NUMBER_OF_RECS
    else:
        number_of_recommendations = int(number_of_recommendations)
    
    sku_list = request.args.get('sku_list')
    if not sku_list:

        # generate random list for recommendations
        number_of_recommendations = random.randrange(1, hyperparams.DEFAULT_NUMBER_OF_RECS)
        rand = random.sample(range(get_total_item_count()), number_of_recommendations)
        items = SkuItem.query.filter(SkuItem.id.in_(rand)).all()
        requested_sku = [item.sku for item in items]
    else: 
        requested_sku = []
        for item in  re.split('\W+', sku_list):
            if len(item):
                requested_sku.append(item)


    items_cleaned, items_info = get_recommendations_list(requested_sku, number_of_recommendations)
    recommended =  items_cleaned[0:number_of_recommendations]

    # get response data 
    for rec_item in recommended:
        item_info = SkuItem.query.filter(SkuItem.sku.like('%' + str(rec_item['recommended_sku']) + '%')).first()
        rec_item.update( {"item_info":item_info})

    context = {}
    context['request'] = items_info
    context['response'] = recommended
    context['num_of_recs'] = number_of_recommendations

    return render_template('recommendations_check.html', **context) 

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

if __name__ == '__main__':
    app.run(host=app.config['HOST_NAME'], port=app.config['PORT'],  debug=app.config['DEBUG'])
