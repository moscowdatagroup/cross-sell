#!/usr/bin/env python
import argparse
import json
import logging
import os
from collections import Counter

import pandas as pd

from cross_sell.config import default as settings
from cross_sell.config import hyperparams
from cross_sell.utils.sql_scripts import categories_data_sql, orders_sql, brand_sql, visibility_sql, sku_data_sql
from cross_sell.utils.train_helpers import (
    cat_sign, collect_categories, conv_to_sku, return_pairs,
    create_category_pair, count_top_n
)
from igraph import Graph
from sklearn.feature_extraction.text import CountVectorizer
from sqlalchemy import create_engine


logging.basicConfig(level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(message)s')
logger = logging.getLogger(__name__)


parser = argparse.ArgumentParser(description='Train models and store them in files')
args = parser.parse_args()


class CrossSellingModel:
    def __init__(self, engine):
        self.sku_data = None
        self.categories_data = None
        self.orders = None
        self.category_tree_whole = None
        self.cat_level = {}
        self.sku_apriori = {}
        self.sku_graph = {}
        self.cat_graph = {}
        self.orders_visible  = None
        self.pairs = Counter()
        self.categories_pairs = {}
        self.categories_apriori = {}
        self.cnt = CountVectorizer()
        self.engine = engine

    def read_data(self):
        '''Load main data structures'''

        logger.info('Loading orders')
        self.orders = pd.read_sql(orders_sql, self.engine)

        logger.info('Loading sku data')
        self.sku_data = pd.read_sql(sku_data_sql, self.engine)

        logger.info('Loading categories data')
        self.categories_data = pd.read_sql(categories_data_sql, self.engine)

        logger.info('Loading category tree')
        self.category_tree_whole = pd.read_csv(settings.CATEGORY_TREE)

        logger.info('Data loading finished')

    def filter_by_visibility(self):
        '''Filter SKU by visibility'''

        logger.info('Loading visibility data')
        visibility = pd.read_sql(visibility_sql, self.engine)
        visibility = visibility[visibility['visible_in_shop'] == 1]

        logger.info('Filtering orders, sku and categories by visibility')
        self.sku_data = self.sku_data[self.sku_data['sku'].isin(visibility['sku'].values)]
        self.orders_visible = self.orders[self.orders['SKUConfig'].isin(visibility['sku'].values)]
        self.categories_data = self.categories_data[self.categories_data['fk_catalog_config'].isin(self.sku_data['id_catalog_config'].values)]

        logger.info('Visibility filtering completed')

    def orders_pre_filtering(self):
        '''Remove outliers from orders'''

        logger.info('Remove orders outliers starting')

        logger.info('Filter by staring date')
        self.orders = (self.orders[self.orders['Date'].apply(
            lambda x: x.year) >= hyperparams.STARTING_YEAR])

        logger.info('Filter too much orders per user')
        user_orders_count = self.orders[['OrderNum','CustomerNum']].groupby('CustomerNum')['OrderNum'].apply(lambda x: set(x)).apply(len)
        user_orders_count_reduced  = user_orders_count[user_orders_count > 1]

        grouped_orders = user_orders_count_reduced.value_counts()
        normalized_frequencies = grouped_orders.apply(float).values/grouped_orders.values.sum()
        density_sum = 0
        for i in range(len(normalized_frequencies)):
            if density_sum > hyperparams.ORDER_FREQUENCY_PROB:
                break
            else:
                density_sum += normalized_frequencies[i]

        MAX_ORDER_VALUE = grouped_orders.index[i]
        logger.debug('Max allowed num of orders: %d', MAX_ORDER_VALUE)

        regular_users = user_orders_count[user_orders_count <= MAX_ORDER_VALUE].index
        self.orders = self.orders[self.orders['CustomerNum'].isin(regular_users)]

        logger.info('Filter too much items in user orders')

        grouped_orders = self.orders[self.orders['ItemsInOrder'] > 1]
        grouped_orders = grouped_orders['ItemsInOrder'].value_counts()
        grouped_orders = grouped_orders.sort_index()
        normalized_frequencies = grouped_orders.apply(float).values/grouped_orders.values.sum()
        density_sum = 0
        for i in range(len(normalized_frequencies)):
            if density_sum > hyperparams.NUM_OF_ORDERS_PROB:
                break
            else:
                density_sum += normalized_frequencies[i]

        MAX_ITEMS_IN_ORDER= grouped_orders.index[i]
        logger.debug('Max items in order: %d', MAX_ITEMS_IN_ORDER)
        self.orders = self.orders[(self.orders['ItemsInOrder'] < MAX_ITEMS_IN_ORDER) & (self.orders['ItemsInOrder'] > 1)]

        logger.info('Orders filtering completed')

    def split_and_clean_categories(self):
        '''Extract and clean flat categories onto cat1-5 levels'''

        logger.info('Preprocess categories data to extract normal categories')
        categories_counts = self.categories_data.groupby('fk_catalog_category').agg(len)
        categories_counts.reset_index(inplace=True)
        categories_counts.columns = ['fk_catalog_category','count']
        joint_categories = pd.merge(self.category_tree_whole, categories_counts, left_on='id', right_on='fk_catalog_category', how = 'left')
        self.cat_level[1] = joint_categories[(joint_categories['cat_lvl'] == 1)
                                             & ((joint_categories['count'] >
                                                 hyperparams.CAT_1_LOW_BORDER))]

        for i in range(2, 6):
            self.cat_level[i] = joint_categories[joint_categories['imm_par'].isin(self.cat_level[i-1]['id'])]
        logger.info('Preprocessing for categories completed')

    def calculate_pairs(self, field):
        '''Create co-occurence pairs'''

        logger.info('Create item co-occurence pairs')
        order_sku_index = self.orders_visible[[field,'SKUConfig']].groupby(field)['SKUConfig'].apply(lambda x: x.tolist()).apply(set).apply(list).values
        self.cnt.fit_transform(self.orders_visible['SKUConfig'])

        logger.info('Starting items renaming')
        replace_all = []
        for i, item in enumerate(order_sku_index):
            replaced = []
            for sku in item:
                replaced.append(self.cnt.vocabulary_[sku.lower()])
            replace_all.append(sorted(replaced))
            if i % 100000 == 0 and i!=0:
                logger.debug('Renaming sku pairs: %d', i)

        logger.info('Starting pairs creating')
        for i, order in enumerate(replace_all):

            self.pairs.update(return_pairs(order))
            if i % 100000 == 0 and i!=0:
                logger.debug('Counting sku pairs: %d', i)

    def apriori_pairs(self, lower_border):
        '''Create apriori pairs'''

        logger.info('Starting apriori calculations')
        logger.info('Filtering pairs creating with frequenct less than: %d', lower_border)
        edges = [(pair[0][0], pair[0][1], pair[1]) for pair in self.pairs.items() if len(pair[0]) > 0 and pair[1] > lower_border]

        sku_row = self.cnt.get_feature_names()
        gr = Graph.TupleList(edges, weights=True)
        items_in_apriori = gr.vs.get_attribute_values('name')

        logger.info('Pairs dictionary creation')
        for item in items_in_apriori:
            pairs = {}
            for pair in edges:
                if pair[0]==item:
                    pairs[sku_row[pair[1]].upper()] = pair[2]
                if pair[1]==item:
                    pairs[sku_row[pair[0]].upper()] = pair[2]
            self.sku_apriori[sku_row[item].upper()] = pairs

        logger.info('Apriori pairs calculations completed')

    def apriori_categories_pair(self, lower_border, category_level):

        logger.info('Starting categories apriori calculations')
        logger.info('Filtering pairs creating with frequenct less than: %d', lower_border)

        edges = [(pair[0][0], pair[0][1], pair[1]) for pair in self.categories_pairs[category_level].items() if len(pair[0]) > 0 and pair[1] > lower_border]

        sku_row = self.cnt.get_feature_names()
        gr = Graph.TupleList(edges, weights=True)
        items_in_apriori = gr.vs.get_attribute_values('name')

        logger.info('Pairs dictionary creation')
        self.categories_apriori[category_level] = {}
        for item in items_in_apriori:
            pairs = {}
            for pair in edges:
                if pair[0]==item:
                    pairs[pair[1]] = pair[2]
                if pair[1]==item:
                    pairs[pair[0]] = pair[2]
            self.categories_apriori[category_level][item] = pairs

        logger.info('Apriori for categories pairs calculations completed')

    def create_sku_graph(self, lower_border):
        '''Create graph recs for sku'''

        logger.info('Starting sku graph processing')

        edges = [(pair[0][0], pair[0][1], pair[1]) for pair in self.pairs.items() if len(pair[0]) > 0 and pair[1] > lower_border]
        weights = [pair[2] for pair in edges]
        gr = Graph.TupleList(edges, weights=True)

        skus_map = self.cnt.get_feature_names()

        logger.info('Create categories and sku maps')

        top_lvl_categories = self.category_tree_whole[self.category_tree_whole['cat_lvl'].isin([0])]['id'].values
        categories_data_to_map = self.categories_data[~self.categories_data['fk_catalog_category'].isin(top_lvl_categories)]
        categories_data_to_map = categories_data_to_map.groupby('fk_catalog_config')['fk_catalog_category'].apply(lambda x: x.tolist())
        sku_category_map = dict(zip(self.sku_data['sku'].apply(str).apply(str.lower), categories_data_to_map))

        logger.info('Starting recursive graph communities search')
        base_clustering = gr.community_fastgreedy(weights = weights)
        base_communities_stack = set(base_clustering.as_clustering().subgraphs())
        small_clusters = []

        while len(base_communities_stack):

            # select subgraph communty and process it iterative until we can't split more
            to_process = base_communities_stack.pop()
            sku_list = conv_to_sku(to_process.vs.get_attribute_values('name'), skus_map)
            sku_categories = collect_categories(sku_list, sku_category_map)
            sku_categories_sign = cat_sign(sku_categories, self.cat_level[1]['id'].values)

            if (len(sku_categories_sign) > hyperparams.CAT_SIGN_LEN and
                    len(sku_list) > hyperparams.SKU_LEN_IN_GRAPH):

                subgr_res = to_process.community_fastgreedy()
                # check if we can split more current graph

                if len(subgr_res.as_clustering().subgraphs()) > 1:
                    base_communities_stack.update(subgr_res.as_clustering().subgraphs())
                else:
                    small_clusters.append(to_process)
            else:
                small_clusters.append(to_process)

        logger.info('Recursive graph communities processing completed. Total communities: %d', len(small_clusters))


        logger.info('Prettify graph')

        for i, sub_gr in enumerate(small_clusters):

            # get subgraph vertexes
            sku_ids = sub_gr.vs.get_attribute_values('name')
            # get subgraph degrees
            sku_degrees = sub_gr.vs.degree()
            subgraph_df = pd.DataFrame({'degree':sku_degrees, 'ids':sku_ids}).sort_values(by='degree', ascending = 0)
            subgraph_df = subgraph_df.reset_index(drop=True)

            for sku_id in subgraph_df.index:

                # store for every vertex remaining vertexes in subgraph
                remain_sku = set(subgraph_df.index.values)
                remain_sku.discard(sku_id)
                self.sku_graph[skus_map[subgraph_df.loc[sku_id, 'ids']].upper()] = {
                'items': dict(zip([skus_map[item].upper() for item in subgraph_df.ix[remain_sku,'ids']], subgraph_df.ix[remain_sku,'degree'])),
                                                 'degree': subgraph_df.loc[sku_id, 'degree'] }

        logger.info('SKU graph processing completed')

    def create_categories_graph(self, category_level, lower_border, field):

        categories_lvl = self.categories_data[self.categories_data['fk_catalog_category'].isin(self.cat_level[category_level]['id'])]
        sku_data_slice = self.sku_data[self.sku_data['id_catalog_config'].isin(categories_lvl['fk_catalog_config'].values)]
        orders_slice = self.orders_visible [self.orders_visible ['SKUConfig'].isin(sku_data_slice['sku'].values)]
        sku_data_slice = pd.merge(orders_slice, sku_data_slice[['sku','id_catalog_config']], left_on='SKUConfig', right_on='sku', how='inner')

        categories_lvl = categories_lvl.groupby('fk_catalog_config')['fk_catalog_category'].apply(lambda x: x.tolist())
        categories_lvl = pd.DataFrame({'catalog_id':categories_lvl.index, 'categories':categories_lvl.values})

        sku_data_slice = pd.merge(sku_data_slice, categories_lvl, left_on = 'id_catalog_config', right_on = 'catalog_id', how = 'inner')

        order_sku_index = orders_slice[[field,'SKUConfig']].groupby(field)['SKUConfig'].apply(lambda x: x.tolist()).apply(set).apply(list).values
        sku_category_map = dict(zip(sku_data_slice['sku'],sku_data_slice['categories'] ) )

        all_pairs = Counter()
        for i, order in enumerate(order_sku_index):
            all_pairs.update(return_pairs(order))
            if i % 100000 == 0 and i!=0:
                logger.debug('Counting sku pairs: %d', i)

        self.categories_pairs[category_level] = Counter()

        #slow function, ~ 470s for 100k items.
        for i, pair in enumerate(all_pairs):
            self.categories_pairs[category_level].update(create_category_pair(pair, all_pairs[pair], sku_category_map))
            if i % 100000 == 0 and i!=0:
                logger.debug('Counting categories pairs: %d', i)

        self.apriori_categories_pair(lower_border, category_level)

        logger.info('Starting categories graph processing')

        edges = [(pair[0][0], pair[0][1], pair[1]) for pair in self.categories_pairs[category_level].items() if len(pair[0]) > 0 and pair[1] > lower_border and pair[0][0] != pair[0][1]]
        weights = [pair[2] for pair in edges]

        logger.info('Starting recursive graph communities search')
        gr = Graph.TupleList(edges, weights=True)
        base_clustering = gr.community_fastgreedy(weights = weights)
        base_communities_stack = set(base_clustering.as_clustering().subgraphs())
        small_clusters = []

        while len(base_communities_stack):

            # select subgraph communty and process it iterative until we can't split more
            to_process = base_communities_stack.pop()
            if len(to_process.vs) > hyperparams.CAT_VARIATIVITY_IN_GRAPH:

                subgr_res = to_process.community_fastgreedy()
                # check if we can split more current graph
                if len(subgr_res.as_clustering().subgraphs()) > 1:
                    base_communities_stack.update(subgr_res.as_clustering().subgraphs())
                else:
                    small_clusters.append(to_process)
            else:
                small_clusters.append(to_process)

        logger.info('Recursive graph communities processing completed. Total communities: %d', len(small_clusters))

        logger.info('Prettify graph')

        self.cat_graph[category_level] = {}
        for i, sub_gr in enumerate(small_clusters):
            cat_ids = sub_gr.vs.get_attribute_values('name')
            cat_degrees = sub_gr.vs.degree()
            subgraph_df = pd.DataFrame({'degree':cat_degrees, 'ids':cat_ids}).sort_values(by='degree', ascending = 0)
            subgraph_df = subgraph_df.reset_index(drop=True)
            for cat_id in subgraph_df.index:
                remain_cats = set(subgraph_df.index.values)
                remain_cats.discard(cat_id)
                self.cat_graph[category_level] [subgraph_df.loc[cat_id, 'ids']] = {'cats': dict(zip(subgraph_df.ix[remain_cats,'ids'], subgraph_df.ix[remain_cats,'degree'])),
                                                 'degree': subgraph_df.loc[cat_id, 'degree'] }

        logger.info('Category lvl %d graph processing completed', category_level)

    def create_categories_map(self):
        '''Create category names map '''

        category_names_map = pd.DataFrame()
        for i in range(1, 5):
            cat_level = self.cat_level[i]
            category_names_map = pd.concat([category_names_map, cat_level[['id', 'name']]], axis=0)

        return dict(zip(category_names_map['id'], category_names_map['name']))

    def calculate_topsellers(self):
        '''Create dictionary for categories and topsellers'''

        all_orders_sku = self.orders['SKUConfig'].unique()
        sku_in_orders = self.sku_data[self.sku_data['sku'].isin(all_orders_sku)]['id_catalog_config']

        top_sellers = self.orders[['SKUConfig','ItemsInOrder']].groupby('SKUConfig').agg(len)
        top_sellers = dict(zip(top_sellers.index, top_sellers['ItemsInOrder']))
        categories_data_for_active = self.categories_data[self.categories_data['fk_catalog_config'].isin(sku_in_orders)]
        categories_data_for_active = pd.merge(categories_data_for_active, self.sku_data[['sku','id_catalog_config']], left_on = 'fk_catalog_config', right_on = 'id_catalog_config')
        categories_sales = categories_data_for_active.groupby('fk_catalog_category')['sku'].apply(lambda x: x.tolist())

        top_sellers_dictionary = categories_sales.apply(count_top_n, top_sellers = top_sellers)

        return dict(zip(top_sellers_dictionary.index, top_sellers_dictionary.values))


    def create_sku_descriptions_to_save(self):
        '''Add extra information to sku data'''

        logger.info('Enrich sku data for saving')
        sku_data_to_save = self.sku_data[['sku','name','id_catalog_config']]
        for cat_level_to_save in range(1,5):
            cat_slice = self.categories_data[self.categories_data['fk_catalog_category'].isin(self.cat_level[cat_level_to_save]['id'].values)]
            cat_slice = cat_slice.groupby('fk_catalog_config')['fk_catalog_category'].apply(lambda x: x.tolist())
            cat_slice = pd.DataFrame({'catalog_id':cat_slice.index, 'cat_level_{lvl}'.format(lvl = cat_level_to_save):cat_slice.values})
            sku_data_to_save = pd.merge(sku_data_to_save, cat_slice, left_on='id_catalog_config', right_on='catalog_id', how='left')

        sku_data_to_save = sku_data_to_save.drop([name for name in sku_data_to_save.keys() if name.find('catalog_id')!=-1], axis=1)

        logger.info('Collecting images')
        brand = pd.read_sql(brand_sql, engine)
        to_image = pd.merge(self.sku_data[['sku','id_catalog_config','fk_catalog_brand']], brand, left_on = 'fk_catalog_brand', right_on = 'id_catalog_brand', how = 'left')
        images = 'http://media.linio.com.mx/p/' + to_image['url_key']+ '-'+ to_image['id_catalog_config'].apply(str).apply(lambda x: x[::-1]) + '-1-zoom.jpg'
        sku_data_to_save['image_url'] = images

        return sku_data_to_save

    def save_model(self):
        '''Save model'''

        logger.info('SKU graph saving')
        with open(settings.datadir('sku_graph_map.json'), 'w') as outfile:
            json.dump(self.sku_graph, outfile)

        logger.info('Apriori for sku saving')
        with open(settings.datadir('apriori_for_sku.json'), 'w') as outfile:
            json.dump(self.sku_apriori, outfile)

        logger.info('Categories graph data saving')
        for i in range(1, 5):
            if i in self.cat_graph.keys():
                with open(
                        settings.datadir('cat_graph_map_lvl_{cat_lvl}.json'.format(cat_lvl=i)), 'w') as outfile:
                    json.dump(self.cat_graph[i], outfile)

                with open(
                        settings.datadir('apriori_for_categories_lvl_{cat_lvl}.json'.format(cat_lvl=i)), 'w') as outfile:
                    json.dump(self.categories_apriori[i], outfile)

        logger.info('Generating topsellers')
        top_sellers = self.calculate_topsellers()
        with open(
                settings.datadir('top_sellers_categories_map.json'), 'w') as outfile:
            json.dump(top_sellers, outfile)

        logger.info('Generating categories names map')
        category_names_map = self.create_categories_map()
        with open(settings.datadir('category_map.json'), 'w') as outfile:
            json.dump(category_names_map, outfile)

        logger.info('Generating sku data to save')
        sku_data_to_save = self.create_sku_descriptions_to_save()
        sku_data_to_save.to_csv(settings.datadir('sku_description.csv'),
                                index=False, encoding = 'utf-8')
        logger.info('Saving completed')

    def fit(self):
        '''Fit whole model'''

        logger.info('Start model training')
        self.read_data()
        self.orders_pre_filtering()
        self.filter_by_visibility()
        self.split_and_clean_categories()
        self.calculate_pairs(hyperparams.GROUPING_FIELD)
        self.apriori_pairs(hyperparams.APRIORI_PAIRS_LOW)
        self.create_sku_graph(hyperparams.APRIORI_PAIRS_LOW)
        for i in [1,2,3,4]:
            self.create_categories_graph(i, hyperparams.CAT_GRAPH_LOW_BORDER,
                                         hyperparams.GROUPING_FIELD)
        self.save_model()
        logger.info('Model training completed')


if __name__ == '__main__':
    engine = create_engine(settings.DATABASE_URI)
    model = CrossSellingModel(engine)
    model.fit()
    engine.dispose()
