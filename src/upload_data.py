# coding: utf-8
import argparse

from cross_sell.config import default as settings
from cross_sell.utils.preprocess import create_nice_file
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker


engine = create_engine(settings.DATABASE_URI)

db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

parser = argparse.ArgumentParser(description='Upload files into database')
parser.add_argument('filename', help='Uploaded filename')
args = parser.parse_args()


def upload_preprocessed_file(filename):

	nice_df = create_nice_file(filename)
	nice_df = nice_df[nice_df['name'].notnull()]
	nice_df = nice_df.drop(['id_catalog_config'], axis=1)
	nice_df.columns = ['sku','name','cat_1','cat_2','cat_3','cat_4','image_url']
	nice_df.to_sql('cross_selling_suggests', engine, index=False, chunksize=10000, if_exists='append')



if __name__ == '__main__':
	print args.filename
	upload_preprocessed_file(args.filename)
	engine.dispose()
